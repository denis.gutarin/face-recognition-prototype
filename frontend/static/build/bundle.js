
(function(l, r) { if (l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (window.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.head.appendChild(r) })(window.document);
var app = (function () {
    'use strict';

    function noop() { }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_input_value(input, value) {
        if (value != null || input.value) {
            input.value = value;
        }
    }
    function set_style(node, key, value, important) {
        node.style.setProperty(key, value, important ? 'important' : '');
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error(`Function called outside component initialization`);
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }
    function createEventDispatcher() {
        const component = get_current_component();
        return (type, detail) => {
            const callbacks = component.$$.callbacks[type];
            if (callbacks) {
                // TODO are there situations where events could be dispatched
                // in a server (non-DOM) environment?
                const event = custom_event(type, detail);
                callbacks.slice().forEach(fn => {
                    fn.call(component, event);
                });
            }
        };
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    function flush() {
        const seen_callbacks = new Set();
        do {
            // first, call beforeUpdate functions
            // and update components
            while (dirty_components.length) {
                const component = dirty_components.shift();
                set_current_component(component);
                update(component.$$);
            }
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    callback();
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update($$.dirty);
            run_all($$.before_update);
            $$.fragment && $$.fragment.p($$.dirty, $$.ctx);
            $$.dirty = null;
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }

    const globals = (typeof window !== 'undefined' ? window : global);

    function destroy_block(block, lookup) {
        block.d(1);
        lookup.delete(block.key);
    }
    function outro_and_destroy_block(block, lookup) {
        transition_out(block, 1, 1, () => {
            lookup.delete(block.key);
        });
    }
    function update_keyed_each(old_blocks, changed, get_key, dynamic, ctx, list, lookup, node, destroy, create_each_block, next, get_context) {
        let o = old_blocks.length;
        let n = list.length;
        let i = o;
        const old_indexes = {};
        while (i--)
            old_indexes[old_blocks[i].key] = i;
        const new_blocks = [];
        const new_lookup = new Map();
        const deltas = new Map();
        i = n;
        while (i--) {
            const child_ctx = get_context(ctx, list, i);
            const key = get_key(child_ctx);
            let block = lookup.get(key);
            if (!block) {
                block = create_each_block(key, child_ctx);
                block.c();
            }
            else if (dynamic) {
                block.p(changed, child_ctx);
            }
            new_lookup.set(key, new_blocks[i] = block);
            if (key in old_indexes)
                deltas.set(key, Math.abs(i - old_indexes[key]));
        }
        const will_move = new Set();
        const did_move = new Set();
        function insert(block) {
            transition_in(block, 1);
            block.m(node, next);
            lookup.set(block.key, block);
            next = block.first;
            n--;
        }
        while (o && n) {
            const new_block = new_blocks[n - 1];
            const old_block = old_blocks[o - 1];
            const new_key = new_block.key;
            const old_key = old_block.key;
            if (new_block === old_block) {
                // do nothing
                next = new_block.first;
                o--;
                n--;
            }
            else if (!new_lookup.has(old_key)) {
                // remove old block
                destroy(old_block, lookup);
                o--;
            }
            else if (!lookup.has(new_key) || will_move.has(new_key)) {
                insert(new_block);
            }
            else if (did_move.has(old_key)) {
                o--;
            }
            else if (deltas.get(new_key) > deltas.get(old_key)) {
                did_move.add(new_key);
                insert(new_block);
            }
            else {
                will_move.add(old_key);
                o--;
            }
        }
        while (o--) {
            const old_block = old_blocks[o];
            if (!new_lookup.has(old_block.key))
                destroy(old_block, lookup);
        }
        while (n)
            insert(new_blocks[n - 1]);
        return new_blocks;
    }
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        // onMount happens before the initial afterUpdate
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = {};
        }
    }
    function make_dirty(component, key) {
        if (!component.$$.dirty) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty = blank_object();
        }
        component.$$.dirty[key] = true;
    }
    function init(component, options, instance, create_fragment, not_equal, props) {
        const parent_component = current_component;
        set_current_component(component);
        const prop_values = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty: null
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, prop_values, (key, ret, value = ret) => {
                if ($$.ctx && not_equal($$.ctx[key], $$.ctx[key] = value)) {
                    if ($$.bound[key])
                        $$.bound[key](value);
                    if (ready)
                        make_dirty(component, key);
                }
                return ret;
            })
            : prop_values;
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(children(options.target));
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set() {
            // overridden by instance, if it has props
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, detail));
    }
    function append_dev(target, node) {
        dispatch_dev("SvelteDOMInsert", { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev("SvelteDOMInsert", { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev("SvelteDOMRemove", { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ["capture"] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev("SvelteDOMAddEventListener", { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev("SvelteDOMRemoveEventListener", { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev("SvelteDOMRemoveAttribute", { node, attribute });
        else
            dispatch_dev("SvelteDOMSetAttribute", { node, attribute, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.data === data)
            return;
        dispatch_dev("SvelteDOMSetData", { node: text, data });
        text.data = data;
    }
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error(`'target' is a required option`);
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn(`Component was already destroyed`); // eslint-disable-line no-console
            };
        }
    }

    const API_URL = "http://0.0.0.0:8000/api";

    /* src/Variant.svelte generated by Svelte v3.15.0 */

    const { console: console_1 } = globals;
    const file = "src/Variant.svelte";

    function get_each_context(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.num = list[i];
    	child_ctx.index = i;
    	return child_ctx;
    }

    // (47:12) {#each numbers as num, index (num)}
    function create_each_block(key_1, ctx) {
    	let img;
    	let img_alt_value;
    	let img_src_value;

    	const block = {
    		key: key_1,
    		first: null,
    		c: function create() {
    			img = element("img");
    			attr_dev(img, "alt", img_alt_value = ctx.num);
    			if (img.src !== (img_src_value = ctx.imageUrl + ctx.num + "/0")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "class", "svelte-1kd2x8t");
    			add_location(img, file, 47, 16, 1215);
    			this.first = img;
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, img, anchor);
    		},
    		p: function update(changed, ctx) {
    			if (changed.numbers && img_alt_value !== (img_alt_value = ctx.num)) {
    				attr_dev(img, "alt", img_alt_value);
    			}

    			if (changed.numbers && img.src !== (img_src_value = ctx.imageUrl + ctx.num + "/0")) {
    				attr_dev(img, "src", img_src_value);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(img);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block.name,
    		type: "each",
    		source: "(47:12) {#each numbers as num, index (num)}",
    		ctx
    	});

    	return block;
    }

    // (75:12) {:else}
    function create_else_block(ctx) {
    	let div;
    	let h2;
    	let t0_value = (100 * ctx.count / ctx.all).toFixed(0) + "";
    	let t0;
    	let t1;
    	let t2;

    	const block = {
    		c: function create() {
    			div = element("div");
    			h2 = element("h2");
    			t0 = text(t0_value);
    			t1 = text(" % -  ");
    			t2 = text(ctx.cluster);
    			add_location(h2, file, 76, 20, 2353);
    			attr_dev(div, "class", "w-100");
    			add_location(div, file, 75, 16, 2313);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, h2);
    			append_dev(h2, t0);
    			append_dev(h2, t1);
    			append_dev(h2, t2);
    		},
    		p: function update(changed, ctx) {
    			if ((changed.count || changed.all) && t0_value !== (t0_value = (100 * ctx.count / ctx.all).toFixed(0) + "")) set_data_dev(t0, t0_value);
    			if (changed.cluster) set_data_dev(t2, ctx.cluster);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(75:12) {:else}",
    		ctx
    	});

    	return block;
    }

    // (55:12) {#if editable}
    function create_if_block(ctx) {
    	let div2;
    	let div0;
    	let span;
    	let t0_value = (100 * ctx.count / ctx.all).toFixed(0) + "";
    	let t0;
    	let t1;
    	let t2;
    	let input;
    	let t3;
    	let div1;
    	let button;
    	let dispose;

    	const block = {
    		c: function create() {
    			div2 = element("div");
    			div0 = element("div");
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = text(" %");
    			t2 = space();
    			input = element("input");
    			t3 = space();
    			div1 = element("div");
    			button = element("button");
    			button.textContent = "Выбрать";
    			attr_dev(span, "class", "input-group-text");
    			add_location(span, file, 57, 24, 1534);
    			attr_dev(div0, "class", "input-group-prepend w-10");
    			add_location(div0, file, 56, 20, 1471);
    			attr_dev(input, "type", "text");
    			attr_dev(input, "class", "form-control w-65");
    			attr_dev(input, "placeholder", "Введите актуальное ФИО");
    			add_location(input, file, 59, 20, 1655);
    			attr_dev(button, "class", "btn btn-primary");
    			add_location(button, file, 67, 24, 2045);
    			attr_dev(div1, "class", "input-group-append w-25");
    			add_location(div1, file, 66, 20, 1983);
    			attr_dev(div2, "class", "input-group");
    			add_location(div2, file, 55, 16, 1425);

    			dispose = [
    				listen_dev(input, "input", ctx.input_input_handler),
    				listen_dev(input, "focus", ctx.handleFocus, false, false, false),
    				listen_dev(input, "blur", ctx.handleBlur, false, false, false),
    				listen_dev(button, "click", ctx.handleSend, false, false, false)
    			];
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div2, anchor);
    			append_dev(div2, div0);
    			append_dev(div0, span);
    			append_dev(span, t0);
    			append_dev(span, t1);
    			append_dev(div2, t2);
    			append_dev(div2, input);
    			set_input_value(input, ctx.newCluster);
    			append_dev(div2, t3);
    			append_dev(div2, div1);
    			append_dev(div1, button);
    		},
    		p: function update(changed, ctx) {
    			if ((changed.count || changed.all) && t0_value !== (t0_value = (100 * ctx.count / ctx.all).toFixed(0) + "")) set_data_dev(t0, t0_value);

    			if (changed.newCluster && input.value !== ctx.newCluster) {
    				set_input_value(input, ctx.newCluster);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(55:12) {#if editable}",
    		ctx
    	});

    	return block;
    }

    function create_fragment(ctx) {
    	let div3;
    	let div2;
    	let div0;
    	let img;
    	let img_src_value;
    	let t0;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let t1;
    	let div1;
    	let each_value = ctx.numbers;
    	const get_key = ctx => ctx.num;

    	for (let i = 0; i < each_value.length; i += 1) {
    		let child_ctx = get_each_context(ctx, each_value, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block(key, child_ctx));
    	}

    	function select_block_type(changed, ctx) {
    		if (ctx.editable) return create_if_block;
    		return create_else_block;
    	}

    	let current_block_type = select_block_type(null, ctx);
    	let if_block = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			div3 = element("div");
    			div2 = element("div");
    			div0 = element("div");
    			img = element("img");
    			t0 = space();

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t1 = space();
    			div1 = element("div");
    			if_block.c();
    			attr_dev(img, "alt", "Новый");
    			if (img.src !== (img_src_value = ctx.imageUrl + "1/1")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "class", "svelte-1kd2x8t");
    			add_location(img, file, 42, 12, 1058);
    			attr_dev(div0, "class", "row mb-1");
    			add_location(div0, file, 41, 8, 1023);
    			attr_dev(div1, "class", "row");
    			add_location(div1, file, 53, 8, 1364);
    			attr_dev(div2, "class", "container");
    			add_location(div2, file, 40, 4, 991);
    			attr_dev(div3, "class", "row m-2 mb-4");
    			set_style(div3, "max-width", "540px");
    			add_location(div3, file, 39, 0, 934);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div2);
    			append_dev(div2, div0);
    			append_dev(div0, img);
    			append_dev(div0, t0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div0, null);
    			}

    			append_dev(div2, t1);
    			append_dev(div2, div1);
    			if_block.m(div1, null);
    		},
    		p: function update(changed, ctx) {
    			const each_value = ctx.numbers;
    			each_blocks = update_keyed_each(each_blocks, changed, get_key, 1, ctx, each_value, each_1_lookup, div0, destroy_block, create_each_block, null, get_each_context);

    			if (current_block_type === (current_block_type = select_block_type(changed, ctx)) && if_block) {
    				if_block.p(changed, ctx);
    			} else {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(div1, null);
    				}
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div3);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}

    			if_block.d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    let sendUrl = "";

    function instance($$self, $$props, $$invalidate) {
    	let { cluster = "" } = $$props;
    	let { count = 0 } = $$props;
    	let { numbers = [] } = $$props;
    	let { all = 0 } = $$props;
    	const dispatch = createEventDispatcher();
    	let newCluster = cluster;
    	let editable = true;
    	let imageUrl = `${API_URL}/photo/${cluster}/`;

    	async function handleSend() {
    		console.log(`Sending: ${cluster}/${newCluster}`);
    		let sendUrl = `${API_URL}/right/${cluster}/${newCluster}`;
    		dispatch("datasending", { cluster, newCluster });
    		await fetch(sendUrl, { method: "POST" });
    		dispatch("datasent", { cluster, newCluster });
    	}

    	function handleFocus() {
    		dispatch("editing", true);
    	}

    	function handleBlur() {
    		dispatch("editing", false);
    	}

    	const writable_props = ["cluster", "count", "numbers", "all"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1.warn(`<Variant> was created with unknown prop '${key}'`);
    	});

    	function input_input_handler() {
    		newCluster = this.value;
    		$$invalidate("newCluster", newCluster);
    	}

    	$$self.$set = $$props => {
    		if ("cluster" in $$props) $$invalidate("cluster", cluster = $$props.cluster);
    		if ("count" in $$props) $$invalidate("count", count = $$props.count);
    		if ("numbers" in $$props) $$invalidate("numbers", numbers = $$props.numbers);
    		if ("all" in $$props) $$invalidate("all", all = $$props.all);
    	};

    	$$self.$capture_state = () => {
    		return {
    			cluster,
    			count,
    			numbers,
    			all,
    			newCluster,
    			editable,
    			imageUrl,
    			sendUrl
    		};
    	};

    	$$self.$inject_state = $$props => {
    		if ("cluster" in $$props) $$invalidate("cluster", cluster = $$props.cluster);
    		if ("count" in $$props) $$invalidate("count", count = $$props.count);
    		if ("numbers" in $$props) $$invalidate("numbers", numbers = $$props.numbers);
    		if ("all" in $$props) $$invalidate("all", all = $$props.all);
    		if ("newCluster" in $$props) $$invalidate("newCluster", newCluster = $$props.newCluster);
    		if ("editable" in $$props) $$invalidate("editable", editable = $$props.editable);
    		if ("imageUrl" in $$props) $$invalidate("imageUrl", imageUrl = $$props.imageUrl);
    		if ("sendUrl" in $$props) sendUrl = $$props.sendUrl;
    	};

    	return {
    		cluster,
    		count,
    		numbers,
    		all,
    		newCluster,
    		editable,
    		imageUrl,
    		handleSend,
    		handleFocus,
    		handleBlur,
    		input_input_handler
    	};
    }

    class Variant extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, { cluster: 0, count: 0, numbers: 0, all: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Variant",
    			options,
    			id: create_fragment.name
    		});
    	}

    	get cluster() {
    		throw new Error("<Variant>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set cluster(value) {
    		throw new Error("<Variant>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get count() {
    		throw new Error("<Variant>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set count(value) {
    		throw new Error("<Variant>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get numbers() {
    		throw new Error("<Variant>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set numbers(value) {
    		throw new Error("<Variant>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get all() {
    		throw new Error("<Variant>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set all(value) {
    		throw new Error("<Variant>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/App.svelte generated by Svelte v3.15.0 */
    const file$1 = "src/App.svelte";

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.variant = list[i];
    	child_ctx.index = i;
    	return child_ctx;
    }

    // (91:8) {:else}
    function create_else_block$1(ctx) {
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_1_anchor;
    	let current;
    	let each_value = ctx.variants;
    	const get_key = ctx => ctx.variant[0];

    	for (let i = 0; i < each_value.length; i += 1) {
    		let child_ctx = get_each_context$1(ctx, each_value, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block$1(key, child_ctx));
    	}

    	const block = {
    		c: function create() {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert_dev(target, each_1_anchor, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			const each_value = ctx.variants;
    			group_outros();
    			each_blocks = update_keyed_each(each_blocks, changed, get_key, 1, ctx, each_value, each_1_lookup, each_1_anchor.parentNode, outro_and_destroy_block, create_each_block$1, each_1_anchor, get_each_context$1);
    			check_outros();
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d(detaching);
    			}

    			if (detaching) detach_dev(each_1_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$1.name,
    		type: "else",
    		source: "(91:8) {:else}",
    		ctx
    	});

    	return block;
    }

    // (87:40) 
    function create_if_block_1(ctx) {
    	let div;
    	let h2;

    	const block = {
    		c: function create() {
    			div = element("div");
    			h2 = element("h2");
    			h2.textContent = "Не нашел людей перед камерой";
    			add_location(h2, file$1, 88, 16, 2237);
    			attr_dev(div, "class", "row p-2 pb-3");
    			add_location(div, file$1, 87, 12, 2194);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, h2);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1.name,
    		type: "if",
    		source: "(87:40) ",
    		ctx
    	});

    	return block;
    }

    // (79:8) {#if sending}
    function create_if_block$1(ctx) {
    	let div;
    	let h2;

    	const block = {
    		c: function create() {
    			div = element("div");
    			h2 = element("h2");
    			h2.textContent = "Обработка...";
    			add_location(h2, file$1, 80, 16, 1859);
    			attr_dev(div, "class", "row p-2 pb-3");
    			add_location(div, file$1, 79, 12, 1816);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, h2);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$1.name,
    		type: "if",
    		source: "(79:8) {#if sending}",
    		ctx
    	});

    	return block;
    }

    // (126:12) {#each variants as variant, index(variant[0]) }
    function create_each_block$1(key_1, ctx) {
    	let first_1;
    	let current;

    	const variant = new Variant({
    			props: {
    				cluster: ctx.variant[0],
    				count: ctx.variant[1],
    				numbers: ctx.randomInts,
    				all: ctx.all
    			},
    			$$inline: true
    		});

    	variant.$on("editing", ctx.handleEditing);
    	variant.$on("datasent", ctx.handleSent);
    	variant.$on("datasending", ctx.handleSending);

    	const block = {
    		key: key_1,
    		first: null,
    		c: function create() {
    			first_1 = empty();
    			create_component(variant.$$.fragment);
    			this.first = first_1;
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, first_1, anchor);
    			mount_component(variant, target, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			const variant_changes = {};
    			if (changed.variants) variant_changes.cluster = ctx.variant[0];
    			if (changed.variants) variant_changes.count = ctx.variant[1];
    			if (changed.randomInts) variant_changes.numbers = ctx.randomInts;
    			if (changed.all) variant_changes.all = ctx.all;
    			variant.$set(variant_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(variant.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(variant.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(first_1);
    			destroy_component(variant, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$1.name,
    		type: "each",
    		source: "(126:12) {#each variants as variant, index(variant[0]) }",
    		ctx
    	});

    	return block;
    }

    function create_fragment$1(ctx) {
    	let main;
    	let div2;
    	let div1;
    	let h1;
    	let t1;
    	let div0;
    	let t2;
    	let t3;
    	let current_block_type_index;
    	let if_block;
    	let t4;
    	let div3;
    	let figure;
    	let img;
    	let img_src_value;
    	let current;
    	const if_block_creators = [create_if_block$1, create_if_block_1, create_else_block$1];
    	const if_blocks = [];

    	function select_block_type(changed, ctx) {
    		if (ctx.sending) return 0;
    		if (ctx.variants.length === 0) return 1;
    		return 2;
    	}

    	current_block_type_index = select_block_type(null, ctx);
    	if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

    	const block = {
    		c: function create() {
    			main = element("main");
    			div2 = element("div");
    			div1 = element("div");
    			h1 = element("h1");
    			h1.textContent = "Face ID";
    			t1 = space();
    			div0 = element("div");
    			t2 = text(ctx.all);
    			t3 = space();
    			if_block.c();
    			t4 = space();
    			div3 = element("div");
    			figure = element("figure");
    			img = element("img");
    			add_location(h1, file$1, 75, 12, 1721);
    			add_location(div0, file$1, 76, 12, 1750);
    			attr_dev(div1, "class", "row p-2 pb-3");
    			add_location(div1, file$1, 74, 8, 1682);
    			attr_dev(div2, "class", "container p-2");
    			add_location(div2, file$1, 73, 4, 1646);
    			if (img.src !== (img_src_value = "http://0.0.0.0:8000/webcam")) attr_dev(img, "src", img_src_value);
    			add_location(img, file$1, 143, 4, 4648);
    			add_location(figure, file$1, 142, 3, 4635);
    			add_location(div3, file$1, 141, 3, 4626);
    			attr_dev(main, "class", "svelte-8zaecl");
    			add_location(main, file$1, 72, 0, 1635);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, main, anchor);
    			append_dev(main, div2);
    			append_dev(div2, div1);
    			append_dev(div1, h1);
    			append_dev(div1, t1);
    			append_dev(div1, div0);
    			append_dev(div0, t2);
    			append_dev(div2, t3);
    			if_blocks[current_block_type_index].m(div2, null);
    			append_dev(main, t4);
    			append_dev(main, div3);
    			append_dev(div3, figure);
    			append_dev(figure, img);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			if (!current || changed.all) set_data_dev(t2, ctx.all);
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(changed, ctx);

    			if (current_block_type_index === previous_block_index) {
    				if_blocks[current_block_type_index].p(changed, ctx);
    			} else {
    				group_outros();

    				transition_out(if_blocks[previous_block_index], 1, 1, () => {
    					if_blocks[previous_block_index] = null;
    				});

    				check_outros();
    				if_block = if_blocks[current_block_type_index];

    				if (!if_block) {
    					if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    					if_block.c();
    				}

    				transition_in(if_block, 1);
    				if_block.m(div2, null);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(main);
    			if_blocks[current_block_type_index].d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function get_random() {
    	return [1, 2, 3, 4].map(x => (Math.random() * 2000000000).toFixed(0));
    }

    function instance$1($$self, $$props, $$invalidate) {
    	let all = 0;
    	let variants = [];
    	let updateLocked = false;
    	let sending = false;
    	let first = [];
    	let second = [];
    	let third = [];
    	let randomInts = [];

    	function handleEditing(event) {
    		updateLocked = event.detail;
    	}

    	function handleSending(event) {
    		updateLocked = true;
    		$$invalidate("sending", sending = true);
    	}

    	function handleSent(event) {
    		updateLocked = false;
    		refresh();
    	}

    	async function refresh() {
    		if (!updateLocked) {
    			let response = await fetch(API_URL);
    			let json = await response.json();
    			$$invalidate("sending", sending = false);
    			$$invalidate("all", all = json.all);
    			$$invalidate("variants", variants = json.variants);
    		}
    	}

    	async function refreshImages() {
    		if (!updateLocked) {
    			$$invalidate("randomInts", randomInts = get_random());
    		}
    	}

    	onMount(async () => {
    		$$invalidate("sending", sending = true);
    		refresh();
    		$$invalidate("randomInts", randomInts = get_random());
    		const interval = setInterval(refresh, 1000);
    		const photoIntervel = setInterval(refreshImages, 1000);

    		return () => {
    			clearInterval(interval);
    		};
    	});

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("all" in $$props) $$invalidate("all", all = $$props.all);
    		if ("variants" in $$props) $$invalidate("variants", variants = $$props.variants);
    		if ("updateLocked" in $$props) updateLocked = $$props.updateLocked;
    		if ("sending" in $$props) $$invalidate("sending", sending = $$props.sending);
    		if ("first" in $$props) first = $$props.first;
    		if ("second" in $$props) second = $$props.second;
    		if ("third" in $$props) third = $$props.third;
    		if ("randomInts" in $$props) $$invalidate("randomInts", randomInts = $$props.randomInts);
    	};

    	return {
    		all,
    		variants,
    		sending,
    		randomInts,
    		handleEditing,
    		handleSending,
    		handleSent
    	};
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment$1.name
    		});
    	}
    }

    const app = new App({
        target: document.body,
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
