FROM python:3.7-buster
WORKDIR /usr/src/app
COPY ./requirements.txt /usr/src/app
RUN pip install -r requirements.txt
EXPOSE 8000
CMD /bin/bash ./dev.sh
# RUN apt install -y cmake
RUN pip install opencv-python