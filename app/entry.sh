#!/bin/bash
flask db upgrade
gunicorn --workers 5 --bind 0.0.0.0:8000 wsgi:app