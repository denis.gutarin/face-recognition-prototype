import datetime
import io
import logging
from collections import defaultdict

from flask import render_template, send_file, redirect, jsonify, Response
from webapp import db
from webapp import app
from webapp.models import Vector
from webapp.web_cam import WebCamera
import random
from flask import send_file

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                    level=logging.INFO)


# @app.after_request
# def add_header(r):
#     r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
#     r.headers["Pragma"] = "no-cache"
#     r.headers["Expires"] = "0"
#     r.headers['Cache-Control'] = 'public, max-age=0'
#     return r



# @app.route('/')
# def video_stream2():
#     return Response(web_cam_frames_generator(WebCamera()),
#                     mimetype='multipart/x-mixed-replace; boundary=frame')



@app.route("/", methods=["GET"])
def index():
    return app.send_static_file("index.html")


@app.route("/api/photo/<string:cluster>/<int:counter>/<int:etalon>", methods=["GET"])
def photo(cluster, counter, etalon):
    if etalon == 1:
        photo = (db.session.query(Vector.image)
                 .filter(Vector.cluster == cluster)
                 .filter(Vector.status == 1)
                 .filter(Vector.image != None)
                 .order_by(db.func.random())
                 .first())
    else:
        photo = (db.session.query(Vector.image)
                 .filter(Vector.cluster == cluster)
                 .filter(Vector.status == 0)
                 .filter(Vector.image != None)
                 .order_by(db.func.random())
                 .first())

    if photo:
        file = io.BytesIO(photo[0])
        return send_file(file, mimetype="image/jpeg")
    else:
        return send_file(io.BytesIO(), mimetype="image/jpeg")


@app.route("/api", methods=["GET"])
def all():
    delete_old_unlabeled()
    counter = db.func.count(db.func.distinct(Vector.id))
    vectors = (db.session.query(Vector.cluster, counter)
               .filter(Vector.status == 0)
               .group_by(Vector.cluster)
               .order_by(db.desc(counter))
               .all())

    veccount = defaultdict(int)
    for key, val in vectors:
        veccount[key.strip()] += val
    vectors = [(key, val) for key, val in veccount.items()]

    all = sum(x[1] for x in vectors)

    obj = {"all": all, "variants": vectors}
    return jsonify(obj)


def delete_old_unlabeled():
    limit_timestamp = datetime.datetime.utcnow() \
                      - datetime.timedelta(seconds=30.0)
    result = (db.session.query(Vector)
              .filter(Vector.status == 0)
              .filter(Vector.timestamp < limit_timestamp)
              .delete())
    result = (db.session.query(Vector)
              .filter(Vector.cluster.ilike('Unknown%'))
              .filter(Vector.timestamp < limit_timestamp)
              .delete(synchronize_session=False))
    db.session.commit()


def update_vectors_status(cluster, status):
    values = {Vector.status: status, Vector.timestamp: db.func.now()}
    result = (db.session.query(Vector)
              .filter(Vector.status == 0)
              .filter(Vector.cluster == cluster)
              .update(values, synchronize_session=False))
    db.session.commit()


def update_clusters_name(cluster, new_cluster):
    values = {Vector.cluster: new_cluster}
    result = (db.session.query(Vector)
              .filter(Vector.cluster == cluster)
              .filter(Vector.status == 0)
              .update(values, synchronize_session=False))

    db.session.commit()


def delete_another_vectors(cluster):
    result = (db.session.query(Vector)
              .filter(Vector.status == 0)
              .filter(Vector.cluster != cluster)
              .delete())
    db.session.commit()


@app.route("/api/right/<string:cluster>/<string:new_cluster>", methods=["POST"])
def right_person(cluster, new_cluster):
    if cluster != new_cluster:
        update_clusters_name(cluster, new_cluster)
    update_vectors_status(new_cluster, 1)
    delete_another_vectors(cluster)
    delete_another_vectors(new_cluster)

    return jsonify({"done": True})





# # Path for all the static files (compiled JS/CSS, etc.)
# @app.route("/<path:path>")
# def home(path):
#     return send_from_directory('client/public', path)

@app.route("/rand")
def hello():
    return str(random.randint(0, 100))


#
# @app.route("/webcam")
# def hello():
#     return WebCamera().get_frame()
#


# http://0.0.0.0:8000/video_stream


##### Single page via Flask
def web_cam_frames_generator(camera):
    while True:
        yield (b'--frame\r\n'
               b'Content-Type: image/png\r\n\r\n' + camera.get_frame() + b'\r\n\r\n')
@app.route('/webcam')
def video_stream():
    return Response(web_cam_frames_generator(WebCamera(cam_type='ip_transa')),
                    mimetype='multipart/x-mixed-replace; boundary=frame')