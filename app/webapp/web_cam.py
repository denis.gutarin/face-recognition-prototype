import cv2
# from PIL import Image
# import io
import numpy as np


class WebCamera(object):
    def __init__(self, cam_type):
        self.cam_type = cam_type
        self.r = {
            'x0': 220,
            'y0': 140,
            'x1': 420,
            'y1': 340
            }
        self.detector = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
        self.color = 150
        self.croped_face = np.array([0])
        if self.cam_type == 'video':
            self.video = cv2.VideoCapture('/usr/src/app/video/VideoSeq-11.14.2019--13.39.08.wmv')
        elif self.cam_type == 'ip_transa':
            self.video = cv2.VideoCapture(0)
            self.video.open("rtsp://admin:h12345678@10.0.39.190:554/Streaming/channels/2/")


    def __del__(self):
        self.video.release()

    # def get_frame(self):
    #     success, frame = self.video.read()
    #
    #     if success:
    #         cv2.rectangle(frame, (self.region['x0'], self.region['y0']),
    #                              (self.region['x1'], self.region['y1']), (0, 0, 255), 4)
    #
    #         ret, jpeg = cv2.imencode('.jpg', frame)
    #
    #         return jpeg.tobytes()

    def get_frame(self):
        success, frame = self.video.read()
        if success:
            cv2.rectangle(frame, (self.r['x0'], self.r['y0']),
                          (self.r['x1'], self.r['y1']), (0, 0, self.color), 10)
            croped_frame = frame[self.r['y0']:self.r['y1'], self.r['x0']:self.r['x1']]
            faces = self.detector.detectMultiScale(cv2.cvtColor(croped_frame, cv2.COLOR_BGR2GRAY),
                                              scaleFactor=1.3, minNeighbors=5, minSize=(70, 70))  #
            if len(faces) != 0:
                for (x, y, w, h) in faces:
                    self.croped_face = cv2.cvtColor(croped_frame, cv2.COLOR_BGR2RGB)[y:y + h, x:x + w]
                    # im = Image.fromarray(self.croped_face)
                    # iobj = io.BytesIO()
                    # im.save(iobj, format="PNG")

            if len(self.croped_face.shape) != 1:
                frame[0:self.croped_face.shape[0], 0:self.croped_face.shape[1], :] = cv2.cvtColor(self.croped_face, cv2.COLOR_RGB2BGR)

            if len(faces) != 0:
                if (self.color < 240):
                    self.color += 10
            else:
                if (self.color > 10):
                    self.color -= 10

            ret, jpeg = cv2.imencode('.jpg', cv2.flip(frame, 1))
            return jpeg.tobytes()

    #
    # cap.release()
    # cv2.destroyAllWindows()
