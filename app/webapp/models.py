from webapp import db
from sqlalchemy.sql import func


class Vector(db.Model):
    """Face embedding vector"""
    __tablename__ = "vectors"

    id = db.Column(db.Integer,
                   primary_key=True)
    cluster = db.Column(db.VARCHAR(500),
                        nullable=False,
                        index=True)
    image = db.Column(db.LargeBinary)
    status = db.Column(db.Integer,
                       nullable=False,
                       default=0,
                       index=True)
    timestamp = db.Column(db.DateTime,
                          nullable=False,
                          index=True,
                          server_default=func.now())
    coords = db.Column(db.ARRAY(db.Float))
