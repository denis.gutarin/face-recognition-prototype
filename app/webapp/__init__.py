from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__,
            instance_relative_config=False,
            static_folder="../static",
            static_url_path="")
cors = CORS(app, resources={r"/*": {"origins": "*"}})

if app.config["ENV"] != "development":
    app.config.from_object("config.ProductionConfig")
else:
    app.config.from_object("config.DevelopmentConfig")

db = SQLAlchemy(app)
migrate = Migrate(app, db)

from webapp import routes, models

if __name__ == '__main__':
    app.run("0.0.0.0")
