FROM python:3.7-buster
WORKDIR /usr/src/app
RUN pip install psycopg2-binary
RUN pip install sqlalchemy

COPY ./loader.py ./main.py
EXPOSE 5555
CMD python main.py