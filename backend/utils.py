import numpy as np
import sqlalchemy as db

from annoy import AnnoyIndex
from collections import Counter, defaultdict
from sklearn.neighbors import NearestNeighbors
from sqlalchemy import func

# -------------------------------------------------------------------
# Parameters
# -------------------------------------------------------------------
LIDER_TRESHOLD = 0.5


# -------------------------------------------------------------------
# Help functions
# -------------------------------------------------------------------

def write_db(connection, table, values):
    query = db.insert(table)
    connection.execute(query, values)


def read_train(conn, table):
    s = db.select([table.c.cluster,
                   table.c.coords
                   ]).where(table.c.status.in_((1, 2)))
    return conn.execute(s)


def train_renewal(conn, table, timestamp):
    s = db.select([table.c.cluster,
                   table.c.coords
                   ]).where(table.c.status == 1).where(table.c.timestamp > timestamp)
    t = db.select([func.max(table.c.timestamp)
                   ]).where(table.c.status == 1).where(table.c.timestamp > timestamp)
    for row in conn.execute(t):
        tt = row[0]
    return conn.execute(s), tt


def get_nearest(train, test, dist_tresholds=[1], annoy=None):
    arr = np.array(train['names'])
    acc_count = {}
    if annoy:
        neigh_dict = Counter()
        for i in dist_tresholds:
            for vector in test:
                nns, dist = annoy.get_nns_by_vector(vector, 3, include_distances=True)  # , search_k=-1
                nns = [n for n, d in zip(nns, dist) if d < i]
                neigh_dict.update(Counter(arr[nns]))
            try:
                winner = neigh_dict.most_common()[0][0]
                if neigh_dict.most_common()[0][1] < LIDER_TRESHOLD * len(test):
                    winner = 'Unknown'
            except:
                winner = 'Unknown'
            acc_count[i] = winner
    else:
        neigh = NearestNeighbors()
        neigh.fit(train['embeddings'])
        for i in dist_tresholds:
            neigh_dict = Counter()
            dist, nns = neigh.kneighbors(test, 3, return_distance=True)
            neigh_dict.update(Counter(arr[nns[np.where(dist < i)]]))
            try:
                winner = neigh_dict.most_common()[0][0]
                if neigh_dict.most_common()[0][1] < LIDER_TRESHOLD * len(test):
                    winner = 'Unknown'
            except:
                winner = 'Unknown'
            acc_count[i] = winner

    return acc_count


def accuracy(predict, dist_treshholds):
    acc_count = defaultdict(int)
    for pred in predict:
        for i in dist_treshholds:
            acc_count[i] += (pred['y'] == pred['y_hat'][i])
    for i in acc_count.keys():
        acc_count[i] = round(acc_count[i] / len(predict), 3)
    return acc_count


def make_annoy(vectors, tree_count=20, path='./output/test.annoy', save=True):
    vector_size = len(vectors[0])
    t = AnnoyIndex(vector_size, 'angular')
    for i, v in enumerate(vectors):
        t.add_item(i, v)
    t.build(tree_count)
    if save:
        pass
        # t.save(path)
    return t
