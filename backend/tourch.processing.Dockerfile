FROM nvidia/cuda
WORKDIR /usr/src/app
RUN apt update
RUN apt-get -y update \
    && apt-get install -y software-properties-common \
    && apt-get -y update \
    && add-apt-repository universe
RUN apt-get -y update
RUN apt-get -y install python3
RUN apt-get -y install python3-pip
RUN apt install -y cmake
RUN pip3 install torch
RUN pip3 install torchvision
RUN pip3 install facenet-pytorch
RUN pip3 install zmq
RUN pip3 install sqlalchemy
RUN pip3 install annoy
RUN pip3 install scikit-learn
RUN pip3 install psycopg2-binary

COPY ./batch_processing_512.py ./main.py
COPY ./utils.py ./
CMD python3 main.py
