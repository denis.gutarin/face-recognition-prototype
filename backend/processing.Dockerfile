FROM python:3.7-buster
WORKDIR /usr/src/app
RUN apt update
RUN apt install -y cmake
RUN pip install zmq
RUN pip install cython
RUN pip install dlib
RUN pip install face_recognition
RUN pip install sqlalchemy
RUN pip install annoy
RUN pip install scikit-learn
RUN pip install psycopg2
RUN pip install opencv-python

COPY ./batch_processing_128.py ./main.py
COPY ./utils.py ./
CMD python main.py
