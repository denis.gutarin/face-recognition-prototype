import face_recognition
import cv2
import os
import re
import sqlalchemy as db

def process_write_folder(folder, Vector):
    train = []
    bad_imgs = []
    for img_id in os.listdir(folder):
        route = os.path.join(folder, img_id)
        im = cv2.imread(route)
        rgb = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        if len(face_recognition.face_encodings(rgb)) == 0:
            bad_imgs += [route]
            print('Im here')
            cv2.imshow(route)
            cv2.waitKey(1)
        else:
            enc = face_recognition.face_encodings(rgb)[0]
            name = re.sub('[\d_\+-]', '', dirr).strip()
            with open(route, 'rb') as f_img:
               bytes_ = f_img.read()
            train += [{'image': bytes_,
                       'status': 1,
                       'coords': enc,
                       'cluster': name}]
            print(len(train))

    query = db.insert(Vector)
    connection.execute(query, train)
    return bad_imgs


path = './samples'
# engine = db.create_engine("postgres://postgres:sddata_123@10.0.16.142:5432/mydb")
# engine = db.create_engine("postgres://postgres:postgres@localhost:5432/vision")
engine = db.create_engine("postgres://postgres@0.0.0.0:5432/postgres")
connection = engine.connect()
metadata = db.MetaData()
Vector = db.Table('vectors', metadata, autoload=True, autoload_with=engine)

bad_images = []
for dirr in os.listdir(path)[:]:
    name = re.sub('[\d_\+-]', '', dirr).strip()
    print(f'Embedding {name}')
    print(dirr)
    bad_images += process_write_folder(os.path.join(path, dirr), Vector)



connection.close()
cv2.destroyAllWindows()