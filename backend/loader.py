import sqlalchemy
import os

first = sqlalchemy.create_engine("postgres://postgres:sddata_123@10.0.16.142:5432/mydb")
# second = sqlalchemy.create_engine("postgres://postgres@0.0.0.0:5432/postgres")
second = sqlalchemy.create_engine(os.environ.get("DATABASE_URL"))
second.execute("delete from vectors")
df = first.execute("select * from vectors")
print('selected')

for x in df:
    print(x[1])
    second.execute("insert into vectors(image, status, cluster, timestamp, coords) values (%s,%s,%s,%s,%s)", x[1:])
