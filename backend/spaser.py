import sqlalchemy as db
import pandas as pd
import numpy as np
import os

from sklearn.cluster import KMeans
from sklearn.metrics import pairwise_distances_argmin_min, pairwise_distances


# engine = db.create_engine("postgres://postgres:sddata_123@10.0.16.142:5432/mydb")
engine = db.create_engine(os.environ.get("DATABASE_URL"))
#engine = db.create_engine("postgres://postgres:postgres@localhost:5433/vision")
connection = engine.connect()
metadata = db.MetaData()
Vector = db.Table('vectors', metadata, autoload=True, autoload_with=engine)
s = db.delete(Vector).where(Vector.columns.cluster.like('Unknown%')).where(Vector.columns.status == 1)
connection.execute(s)

s = db.select([Vector.c.id,
               Vector.c.cluster,
               Vector.c.coords,
               Vector.c.status
               ]).where(Vector.c.status.in_((1, 2)))
Data = pd.read_sql(s, connection)

index_to_del = []
for name, group in Data.groupby('cluster'):
    X = np.array([np.array(x) for x in group['coords'].values])
    if X.shape[0] > 10:
#       mean distance in one label more than 0.89 marks clusters as suspicious (status=3)
        dist = np.mean(pairwise_distances(X, X))
        if dist > 0.5 and set(group['status']) != set({2}):
            s = db.update(Vector).where(Vector.c.id.in_(group['id'])).values(status=3)
            connection.execute(s)

#       for nonsuspicious leave 10 vectors only
        elif set(group['status']) != set({2}):
            kmeans = KMeans(n_clusters=10, random_state=0).fit(X)
            closest_inds, _ = pairwise_distances_argmin_min(kmeans.cluster_centers_, X)
            group_index = group.index
            index_to_del += list(group_index[list(set([i for i in range(group_index.shape[0])]) - set(closest_inds))])

id_to_delete = tuple([int(i) for i in Data.iloc[index_to_del, :].id.values])

query = db.delete(Vector)
query = query.where(Vector.columns.id.in_(id_to_delete))
results = connection.execute(query)
connection.close()
