import argparse
import face_recognition
import time
import zmq
import datetime
import os
import io
from sklearn.cluster import DBSCAN
from PIL import Image
from utils import *
import logging
import cv2
import multiprocessing
import numpy as np
#import psutil

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                    level=logging.INFO)

# from validate import *
#####################################################################
parser = argparse.ArgumentParser()
parser.add_argument('--cropper-port', type=str, default=5555,
                    help='cropper port')
parser.add_argument('--pg-engine', type=str,
                    #default="postgres://postgres:postgres@localhost:5433/vision",
                    help='path to DataBase')
parser.add_argument('--db-conn', type=str, default='localhost',
                    help='path to the database')
parser.add_argument('--annoy', type=str, default='./output/test.annoy',
                    help='path to the annoy index')
args = parser.parse_args()
#####################################################################

CONST_TRESHOLD = 0.3
SIZE = 100


def start_process():
    multiprocessing.current_process().name


def do_calculation(data):
    data = cv2.imdecode(np.frombuffer(data, np.uint8), cv2.IMREAD_COLOR)
    gray = cv2.cvtColor(data, cv2.COLOR_BGR2GRAY)
    rgb = cv2.cvtColor(data, cv2.COLOR_BGR2RGB)
    faces = detector.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=5, minSize=(90, 90))
    out = []
    if len(faces) != 0:
        for (x, y, w, h) in faces:
            croped_face = rgb[y:y + h, x:x + w]
            im = Image.fromarray(croped_face)
            iobj = io.BytesIO()
            im.save(iobj, format="PNG")
            out.append((iobj.getvalue(),
                        face_recognition.face_encodings(croped_face,
                                            known_face_locations=[(0, croped_face.shape[1], croped_face.shape[0], 0)]),
                        ))#cv2.resize(rgb[y:y + h, x:x + w], (SIZE, SIZE))))
            #logging.info(f"cropped_coords: {h}, {w}, {croped_face.shape[1]}, {croped_face.shape[0]}")
    else:
        out = 0
    return out


def test(queue, dist_treshholds=[1]):
    logging.info(f"Queue length: {len(queue)}")
    pool = multiprocessing.Pool(processes=len(queue),
                                initializer=start_process, )
    pool_outputs = pool.map(do_calculation, queue)
    pool.close()  # no more tasks
    pool.join()  # wrap up current tasks
    out = sum([k for k in pool_outputs if k != 0], [])
    embeddings = [k[1][0] for k in out]
    images = [k[0] for k in out]
    if len(embeddings):
        logging.info(f'Embeddings length {len(embeddings)}')
        logging.info("Start clustering")
        clustering = DBSCAN(eps=0.4, min_samples=4).fit(embeddings)
        logging.info("End clustering")
        for c in set(clustering.labels_) - set([-1]):
            logging.info(f"Cluster label {c}")
            c_ind = np.where(np.array(clustering.labels_) == c)
            em = np.array(embeddings)[c_ind]
            winner = get_nearest(train, em, dist_treshholds, annoy_index)
            logging.info("Winner calculated")
            w = winner[CONST_TRESHOLD]
            if w == 'Unknown':
                w = f'Unknown_{datetime.datetime.utcnow()}_{c}'
            values = [{'image': im,
                       'status': 0,
                       'coords': emb,
                       'cluster': w}
                      for im, emb in zip(np.array(images)[c_ind], em)]
            write_db(connection, Vector, values)
            logging.info("Written into DB")


context = zmq.Context()

engine = db.create_engine(args.pg_engine or os.environ.get("DATABASE_URL"))
connection = engine.connect()
metadata = db.MetaData()
Vector = db.Table('vectors', metadata, autoload=True, autoload_with=engine)
detector = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
train = defaultdict(list)
timestamp = datetime.datetime(2001, 1, 1)

with context.socket(zmq.REQ) as socket:
    socket.connect(f"tcp://{os.environ.get('CROPPER_HOST')}:{args.cropper_port}")
    #socket.connect(f"tcp://127.0.0.1:{args.cropper_port}")
    while True:
        try:
            socket.send_string('ready')
            queue = socket.recv_pyobj()
            rows, ts = train_renewal(connection, Vector, timestamp)
            if ts:
                timestamp = ts
                for row in rows:
                    train['embeddings'] += [row[1]]
                    train['names'] += [row[0]]
                print(len(train['names']))
                st = time.time()
                if len(queue):
                    logging.info("Batch received from socket")
                annoy_index = make_annoy(train['embeddings'], path=args.annoy, tree_count=30)
                if len(queue):
                    logging.info("Index updated")
                # print('Make annoy time - ', time.time() - st)

            dist_treshholds = [CONST_TRESHOLD]
            # print(len(queue))
            if len(queue):
                pred = test(queue, dist_treshholds)

        except KeyboardInterrupt:
            print('All done')
            connection.close()
            socket.close()

cv2.destroyAllWindows()