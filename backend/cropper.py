import zmq
from collections import deque
import cv2
import logging
import os

batch_size = int(os.environ.get("BATCH_SIZE"))
logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                    level=logging.INFO,
                    datefmt='%Y-%m-%d %H:%M:%S.%uuu')

#cap = cv2.VideoCapture(0)
cap = cv2.VideoCapture('/usr/src/app/video/VideoSeq-11.14.2019--13.39.08.wmv')
# if os.path.exists('/usr/src/app/video/bottom_cam_12_12.wmv'):
#     logging.info('File exists')
# else:
#     logging.info('No file')
queue = deque([], batch_size)
logging.info(f'cropper here')
detector = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")

port = 5555

context = zmq.Context()
poller = zmq.Poller()

with context.socket(zmq.REP) as socket:
    socket.bind("tcp://*:%s" % port)
    poller.register(socket, zmq.POLLIN)
    frame_counter = 0

    while True:
        has_frame, frame = cap.read()
        if not has_frame:
            break

        # if frame_counter % 100 == 0:
        #     print(frame_counter)
        # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        #
        #
        # faces = detector.detectMultiScale(gray, scaleFactor=1.3, minSize=(50, 50))
        # for (x, y, w, h) in faces:
        #     croped_face = rgb[y:y + h, x:x + w]
        #     # for InceptionResnetV1
        #     # im = Image.fromarray(croped_face)
        #     # queue.append(Image.fromarray(croped_face))
        #     # # logging.info("Image append")
        #     # for face_recognition
        #     queue.append(croped_face)
        #     frame_counter += 1
        # r = requests.get(url, auth=('adm', 'kqY#FLqekGp'), stream=True)
        # file = io.BytesIO()
        # for chunk in r:
        #     file.write(chunk)

## Работающий вывод не через веб
        # cv2.imshow('! Press Q to exit...', frame)
        # if cv2.waitKey(1) & 0xFF == ord('q'):
        #     break
        try:
            #npimage = np.asarray(bytearray(file.getvalue()), dtype=np.uint8)
            frame = cv2.imencode('.jpg', frame)[1].tostring()
            if frame is not None:
                queue.append(frame)
                frame_counter += 1
        except:
            continue
        # file.close()
        if len(queue) >= batch_size:
            if poller.poll(1):
                message = socket.recv_string()
                if message == 'ready':
                    socket.send_pyobj(queue)
                    queue = deque([], batch_size)

cap.release()
cv2.destroyAllWindows()
# context = zmq.Context()
# with context.socket(zmq.REQ) as socket:
#     # socket.connect(f"tcp://192.168.1.7:5555")
#     socket.connect('tcp://10.0.40.126:5555')
#     while True:
#         try:
#             socket.send_string('ready')
#             queue = socket.recv_pyobj()
#             print(len(queue))
#             for im in queue:
#                 frame = cv2.imdecode(np.frombuffer(im, np.uint8), cv2.IMREAD_COLOR)
#                 cv2.imshow('! Press Q to exit...', frame)
#             if cv2.waitKey(1) & 0xFF == ord('q'):
#                 break
#         except:
#             continue
#
# cv2.destroyAllWindows()