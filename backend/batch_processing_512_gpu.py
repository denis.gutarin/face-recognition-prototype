import argparse
import time
import zmq
import datetime
import os
import io
import torch
from sklearn.cluster import DBSCAN
from PIL import Image
from utils import *
from facenet_pytorch import InceptionResnetV1
from torchvision import transforms
from torch.utils.data import Dataset
import logging

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                    level=logging.INFO)

#####################################################################
parser = argparse.ArgumentParser()
parser.add_argument('--cropper-port', type=str, default=5555,
                    help='cropper port')
parser.add_argument('--pg-engine', type=str,
                    help='path to DataBase')
parser.add_argument('--db-conn', type=str, default='localhost',
                    help='path to the database')
parser.add_argument('--annoy', type=str, default='./output/test.annoy',
                    help='path to the annoy index')
args = parser.parse_args()
#####################################################################

CONST_TRESHOLD = 0.75


class Cropped(Dataset):
    def __init__(self, queue, transform=None):
        self.transform = transform
        self.queue = queue

    def __len__(self):
        return len(self.queue)

    def __getitem__(self, index):
        img = self.queue.pop()
        imgByteArr = io.BytesIO()
        img.save(imgByteArr, format='PNG')
        imgByteArr = imgByteArr.getvalue()
        if self.transform:
            img = self.transform(img)

        return img, imgByteArr


device = torch.device("cuda:0")
logging.info(torch.cuda.current_device())
logging.info(torch.cuda.get_device_name(0))


def test(model, test_loader, dist_treshholds=[1]):
    for i_step, (x, res) in enumerate(test_loader):
        st = time.time()
        x = x.to(device)
        embeddings = model(x).cpu().detach().numpy()
        print(time.time() - st)
        logging.info(f"Embedings length: {len(embeddings)}")
        clustering = DBSCAN(eps=0.6, min_samples=7).fit(embeddings)
        for c in set(clustering.labels_) - set([-1]):
            c_ind = np.where(np.array(clustering.labels_) == c)
            em = np.array(embeddings)[c_ind]
            winner = get_nearest(train, em, dist_treshholds, annoy_index)
            w = winner[CONST_TRESHOLD]
            if w == 'Unknown':
                w = f'Unknown_{c}'
            values = [{'image': im,
                       'status': 0,
                       'coords': emb.astype(float),
                       'cluster': w}
                      for im, emb in zip(np.array(res)[c_ind], em)]
            write_db(connection, Vector, values)


context = zmq.Context()

engine = db.create_engine(args.pg_engine or os.environ.get("DATABASE_URL"))
connection = engine.connect()
metadata = db.MetaData()
Vector = db.Table('vectors', metadata, autoload=True, autoload_with=engine)

train = defaultdict(list)
timestamp = datetime.datetime(2001, 1, 1)

with context.socket(zmq.REQ) as socket:
    socket.connect(f"tcp://{os.environ.get('CROPPER_HOST')}:{args.cropper_port}")
    while True:
        try:
            socket.send_string('ready')
            queue = socket.recv_pyobj()
            logging.info(f"Queue length: {len(queue)}")
            test_dataset = Cropped(queue,
                                   transform=transforms.Compose(
                                       [transforms.Resize((160, 160)),
                                        transforms.ToTensor(),
                                        transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                             std=[0.229, 0.224, 0.225])])
                                   )
            batch_size = 20

            test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size)

            rows, ts = train_renewal(connection, Vector, timestamp)
            if ts:
                print(ts)
                timestamp = ts
                for row in rows:
                    train['embeddings'] += [row[1]]
                    train['names'] += [row[0]]
                print(len(train['names']))
                st = time.time()
                if len(queue):
                    logging.info("Batch received from socket")
                annoy_index = make_annoy(train['embeddings'], path=args.annoy, tree_count=30)
                if len(queue):
                    logging.info("Index updated")
                # print('Make annoy time - ', time.time() - st)

            model = InceptionResnetV1(pretrained='vggface2').eval()
            model.to(device)
            dist_treshholds = [CONST_TRESHOLD]
            # print(len(queue))
            if len(queue):
                pred = test(model, test_loader, dist_treshholds)

        except KeyboardInterrupt:
            print('All done')
            connection.close()
            socket.close()
