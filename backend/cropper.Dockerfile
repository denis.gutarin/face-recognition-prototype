FROM python:3.7-buster
WORKDIR /usr/src/app
RUN mkdir /usr/src/app/video
RUN pip install zmq
RUN pip install opencv-python

COPY ./cropper.py ./main.py
EXPOSE 5555
CMD python main.py